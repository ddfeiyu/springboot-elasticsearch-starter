package boot.spring.dto.index.mapping;

import boot.spring.dto.index.Base;
import boot.spring.dto.index.mapping.settings.Analysis;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Settings extends Base {
    private Analysis analysis;

    @Override
    public String toString() {
        return "Settings{" +
                "analysis=" + analysis +
                '}';
    }
}

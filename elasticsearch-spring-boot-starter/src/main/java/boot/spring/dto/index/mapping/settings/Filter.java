package boot.spring.dto.index.mapping.settings;

import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * "my_filter":{
     "type":"stop",
     "stopwords":""
     }
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filter extends Base {

    private CustomizeFilter my_filter;

}

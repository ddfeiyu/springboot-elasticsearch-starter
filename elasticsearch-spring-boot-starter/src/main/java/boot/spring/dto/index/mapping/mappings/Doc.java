package boot.spring.dto.index.mapping.mappings;
import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Doc extends Base {

    private Map<String, Prop> properties;
}

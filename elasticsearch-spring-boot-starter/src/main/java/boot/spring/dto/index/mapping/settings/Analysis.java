package boot.spring.dto.index.mapping.settings;

import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Analysis extends Base {

    private Filter filter;
    private Tokenizer tokenizer;
    private Analyzer analyzer;

    public Analysis(){}
    public Analysis(Filter filter, Tokenizer tokenizer, Analyzer analyzer){
        this.filter = filter;
        this.tokenizer = tokenizer;
        this.analyzer = analyzer;
    }

    @Override
    public String toString() {
        return "Analysis{" +
                "filter=" + filter +
                ", tokenizer=" + tokenizer +
                ", analyzer=" + analyzer +
                '}';
    }
}

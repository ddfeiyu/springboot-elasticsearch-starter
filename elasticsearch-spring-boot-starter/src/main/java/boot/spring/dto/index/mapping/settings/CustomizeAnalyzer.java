package boot.spring.dto.index.mapping.settings;

import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomizeAnalyzer extends Base {

    private String filter;
    private String char_filter;
    private String type;
    private String tokenizer;


}

package boot.spring.dto.index.mapping.mappings;

import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Prop extends Base {

    private String type;
    private String analyzer;
    private String format;
}

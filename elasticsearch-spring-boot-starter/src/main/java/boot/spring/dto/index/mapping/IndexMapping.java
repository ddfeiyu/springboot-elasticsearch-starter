package boot.spring.dto.index.mapping;

import lombok.Data;

/**
 * {
 * "settings": {
 * "analysis": {
 * "filter": {
 * "my_filter": {
 * "type": "stop",
 * "stopwords": ""
 * }
 * },
 * "tokenizer": {
 * "my_tokenizer": {
 * "type": "standard",
 * "max_token_length": "1"
 * }
 * },
 * "analyzer": {
 * "my_analyzer": {
 * "filter": "my_filter",
 * "char_filter": "",
 * "type": "custom",
 * "tokenizer": "my_tokenizer"
 * }
 * }
 * }
 * },
 * "mappings": {
 * "_doc": {
 * "properties": {
 * "id": {
 * "type": "integer"
 * },
 * "clicknum": {
 * "type": "integer"
 * },
 * "keywords": {
 * "type": "text",
 * "analyzer": "my_analyzer"
 * },
 * "rank": {
 * "type": "integer"
 * },
 * "url": {
 * "type": "text",
 * "analyzer": "my_analyzer"
 * },
 * "userid": {
 * "type": "text",
 * "analyzer": "my_analyzer"
 * },
 * "visittime": {
 * "type": "date",
 * "format": "HH:mm:ss"
 * }
 * <p>
 * }
 * }
 * }
 * }
 */

@Data
public class IndexMapping {

    private Settings settings;
    private Mappings mappings;

    public IndexMapping(){}

    public IndexMapping(Settings settings, Mappings mappings){
        this.settings = settings;
        this.mappings = mappings;
    }

    @Override
    public String toString() {
        return "IndexMapping{" +
                "settings=" + settings +
                ", mappings=" + mappings +
                '}';
    }
}

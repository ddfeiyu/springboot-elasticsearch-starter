package boot.spring.dto.index.mapping.settings;

import boot.spring.dto.index.Base;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomizeTokenizer extends Base{

    private String type;
    private String max_token_length;

}

package boot.spring.dto.index.mapping;

import boot.spring.dto.index.Base;
import boot.spring.dto.index.mapping.mappings.Doc;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Mappings extends Base {

    private Doc _doc;
}

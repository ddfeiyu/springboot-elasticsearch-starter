package boot.spring.dto.index;

import boot.spring.dto.index.mapping.IndexMapping;
import boot.spring.dto.index.mapping.Mappings;
import boot.spring.dto.index.mapping.Settings;
import boot.spring.dto.index.mapping.mappings.Doc;
import boot.spring.dto.index.mapping.mappings.Prop;
import boot.spring.dto.index.mapping.settings.*;
import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

public class TestIndexMapping {

    public static void main(String[] args) {

        // settings
        Settings settings = new Settings();
        Mappings mappings = new Mappings();
        //
        IndexMapping indexMapping = new IndexMapping(settings, mappings);


        //
        Analysis analysis = new Analysis();
        settings.setAnalysis(analysis);

        //

         Filter filter = new Filter();
         Tokenizer tokenizer = new Tokenizer();
         Analyzer analyzer = new  Analyzer();

        analysis.setFilter(filter);
        analysis.setAnalyzer(analyzer);
        analysis.setTokenizer(tokenizer);

        // filter
        CustomizeFilter my_filter = new CustomizeFilter("stop", "");
        filter.setMy_filter(my_filter);

        // tokenizer
        CustomizeTokenizer my_tokenizer = new CustomizeTokenizer("standard", "1");
        tokenizer.setMy_tokenizer(my_tokenizer);

        //
        /**
         * "analyzer":{
         "my_analyzer":{
         "filter":"my_filter",
         "char_filter":"",
         "type":"custom",
         "tokenizer":"my_tokenizer"

         */
        CustomizeAnalyzer my_analyzer = new CustomizeAnalyzer(
                "my_filter"
                , ""
                , "custom"
                , "my_tokenizer"
        );
        analyzer.setMy_analyzer(my_analyzer);

        // -------------------------------------
        // mapping
        Doc _doc = new Doc();
        mappings.set_doc(_doc);

        Map<String, Prop> properties = new HashMap<>();
        _doc.setProperties(properties);

        //
        properties.put("id", new Prop("integer", null, null));
        properties.put("clicknum", new Prop("integer", null, null));
        properties.put("keywords", new Prop("text", "my_analyzer", null));
        properties.put("rank", new Prop("text", null, null));
        properties.put("url", new Prop("text", "my_analyzer", null));
        properties.put("userid", new Prop("text", "my_analyzer", null));
        properties.put("visittime", new Prop("date", null, "HH:mm:ss"));


        System.out.println(JSON.toJSONString(indexMapping));
    }
}

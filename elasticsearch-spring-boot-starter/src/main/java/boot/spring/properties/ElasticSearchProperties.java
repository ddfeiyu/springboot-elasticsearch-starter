package boot.spring.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "elasticsearch.cluster", ignoreUnknownFields = true)
public class ElasticSearchProperties {

    private String nodes;
}

package boot.spring.po;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class GalaxyMetadataIndexRequestPo implements Serializable {

    private static final long serialVersionUID = 4626498668562852761L;

    /**
     * keyword
     */
    private Long id;
    /**
     *项目名称 keyword
     */
    private String projectname;
    /**
     * 表英文名称 ：text
     */
    private String tableenname;

    /**
     * 表中文名称 ：text
     */
    private String tablecnname;
    /**
     * 用户登录名 ：text
     */
    private String userloginname;
    /**
     * 访问时间
     */
    private String visittime;

}

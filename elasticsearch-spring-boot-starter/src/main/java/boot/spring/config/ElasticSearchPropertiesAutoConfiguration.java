package boot.spring.config;

import boot.spring.elastic.service.*;
import boot.spring.elastic.service.impl.*;
import boot.spring.properties.ElasticSearchProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@Slf4j
public class ElasticSearchPropertiesAutoConfiguration {

    @Autowired
    private ElasticSearchProperties elasticSearchProperties;

    @Bean
    public ElasticSearchProperties elasticSearchProperties() {
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.elasticSearchProperties");
        return new ElasticSearchProperties();
    }

    @Bean
    public RestHighLevelClient configRestHighLevelClient(){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.configRestHighLevelClient, elasticSearchProperties:[ {} ]", elasticSearchProperties);
        String[] hostsAndPorts = elasticSearchProperties.getNodes().split(",");
        HttpHost[] httpHosts = new HttpHost[hostsAndPorts.length];
        for(int i=0;i<hostsAndPorts.length;i++){
            httpHosts[i] = HttpHost.create(hostsAndPorts[i]);
        }

        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(httpHosts));
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.configRestHighLevelClient");
        return client;
    }

    @Bean
    public IndexService indexService(RestHighLevelClient configRestHighLevelClient){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.indexService, configRestHighLevelClient:[ {} ]", configRestHighLevelClient);
        return new IndexServiceImpl(configRestHighLevelClient);
    }

    @Bean
    public AggsService aggsService(RestHighLevelClient configRestHighLevelClient){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.aggsService, configRestHighLevelClient:[ {} ]", configRestHighLevelClient);
        return new AggsServiceImpl(configRestHighLevelClient);
    }

    @Bean
    public SearchService searchService(RestHighLevelClient configRestHighLevelClient){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.searchService, configRestHighLevelClient:[ {} ]", configRestHighLevelClient);
        return new SearchServiceImpl(configRestHighLevelClient);
    }

    @Bean
    public SimpleSearchService simpleSearchService(RestHighLevelClient configRestHighLevelClient){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.simpleSearchService, configRestHighLevelClient:[ {} ]", configRestHighLevelClient);
        return new SimpleSearchServiceImpl(configRestHighLevelClient);
    }

    @Bean
    public SimpleIndexService simpleIndexService(RestHighLevelClient configRestHighLevelClient){
        log.info("boot.spring.config.ElasticSearchPropertiesAutoConfiguration.simpleIndexService, configRestHighLevelClient:[ {} ]", configRestHighLevelClient);
        return new SimpleIndexServiceImpl(configRestHighLevelClient);
    }
}

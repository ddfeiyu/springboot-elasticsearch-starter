package boot.spring.pagemodel;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;


/**
 * 搜索请求参数
 * @author shenzhanwang
 *
 */
public class ElasticSearchRequest {
	// 查询条件
	private QueryCommand query;
	// 过滤条件
	private FilterCommand filter;
	
	public QueryCommand getQuery() {
		return query;
	}
	public void setQuery(QueryCommand query) {
		this.query = query;
	}
	public FilterCommand getFilter() {
		return filter;
	}
	public void setFilter(FilterCommand filter) {
		this.filter = filter;
	}

	@Override
	public String toString() {
		return "ElasticSearchRequest{" +
				"query=" + query +
				", filter=" + filter +
				'}';
	}

	public static void main1(String[] args) {
		/**
		 // 表名
		 String indexname;
		 //关键词
		 private String keyWords;
		 //搜索域
		 private String search_field;
		 //逻辑连接词
		 private String operator;
		 // 排序
		 String sort;
		 //起始位置
		 private int start;
		 //返回条数
		 private int rows;
		 //返回字段
		 private String return_filed;

		 String startdate;

		 String enddate;
		 // 聚集字段
		 String aggsField;
		 // 步长
		 Integer step;
		 // 滚动分页id
		 String scrollid;

		 */
		QueryCommand query = new QueryCommand();
		query.setIndexname("galaxylog");
		query.setKeyWords("1.456.172929920");
		// 返回个数
		query.setRows(3);
		// 起始位置
		query.setStart((1-1) * query.getRows());
		// 排序
		query.setSort("logtime");
		query.setStep(1);
		// FIXME 第一次请求后拿到scrollid，再去滚动查询
		query.setScrollid("");

		/**
		 String startdate;

		 String enddate;

		 String field;
		 */
		FilterCommand filter = new FilterCommand();
		filter.setStartdate("2020-04-13 14:28:31");
		//filter.setEnddate("00:03:00");
		filter.setField("logtime");

		ElasticSearchRequest request = new ElasticSearchRequest();
		request.setQuery(query);
		request.setFilter(filter);

		System.out.println(JSON.toJSONString(request));


	}
}

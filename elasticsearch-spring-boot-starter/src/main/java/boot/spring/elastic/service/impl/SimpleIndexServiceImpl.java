package boot.spring.elastic.service.impl;

import boot.spring.elastic.service.SimpleIndexService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class SimpleIndexServiceImpl implements SimpleIndexService {


    private RestHighLevelClient client;

    public SimpleIndexServiceImpl(RestHighLevelClient client){
        this.client = client;
    }

    @Override
    public boolean deleteIndex(String indexName) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteIndex——删除索引, indexName:[ {} ] ", indexName  );
        try {
            DeleteIndexRequest request = new DeleteIndexRequest(indexName);
            client.indices().delete(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteIndex——IOException删除一个索引，索引不存在, indexName:[ {} ] ", indexName ,e );
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteIndex——ElasticsearchException 删除一个索引，索引不存在, indexName:[ {} ] ", indexName  );
            }else{
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteIndex——ElasticsearchException 删除一个索引，索引不存在, indexName:[ {} ] ", indexName  );
            }
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteIndex——Exception删除一个索引，索引不存在, indexName:[ {} ] ", indexName ,e );
        }
        return false;
    }

    @Override
    public boolean existIndex(String indexName) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.existIndex——判断索引是否存在, indexName:[ {} ] ", indexName  );
       /*GetIndexRequest request = new GetIndexRequest(indexName);
        try {
            return client.indices().exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.existIndex——判断索引是否存在, indexName:[ {} ] ", indexName ,e );
        }
        return false;*/
       return true;
    }

    @Override
    public boolean createMapping(String indexName, XContentBuilder mapping) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMapping, 创建索引映射, indexName:[ {} ], mapping:[ {} ]", indexName , mapping.prettyPrint());
        try {
            CreateIndexRequest index = new CreateIndexRequest(indexName);
            index.source(mapping);
            client.indices().create(index, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMapping, 创建索引映射, indexName:[ {} ], mapping:[ {} ]", indexName , mapping.prettyPrint(), e);
        }
        return false;
    }

    @Override
    public boolean createMapping(String indexName, String mapping, XContentType  xContentType, String settings, XContentType settingsContentType){
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMapping, 创建索引映射, indexName:[ {} ], mapping:  [ {} ]  , settings:  [ {} ]  ", indexName , mapping, settings);
        try {
            CreateIndexRequest index = new CreateIndexRequest(indexName);
            index.settings(settings, settingsContentType);
            index.mapping(mapping,xContentType);
            client.indices().create(index, RequestOptions.DEFAULT);
            return true;
        } catch (Exception e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMapping, 创建索引映射, indexName:[ {} ], mapping:[ {} ] , settings:  [ {} ]", indexName , mapping, settings, e);
        }
        return false;
    }


    @Override
    public void createMappingAsync(String indexName, XContentBuilder mapping, ActionListener actionListener){
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMappingAsync, 创建索引映射, indexName:[ {} ], mapping:[ {} ]", indexName , mapping.prettyPrint());
        try {
            CreateIndexRequest index = new CreateIndexRequest(indexName);
            index.source(mapping);
            client.indices().createAsync(index, RequestOptions.DEFAULT, actionListener);
        } catch (Exception e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMappingAsync, 创建索引映射, indexName:[ {} ], mapping:[ {} ]", indexName , mapping.prettyPrint(), e);
        }
    }

    @Override
    public void createMappingAsync(String indexName, String mapping, XContentType mappingContentType, String settings, XContentType settingsContentType, ActionListener actionListener) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMappingAsync, 创建索引映射, indexName:[ {} ], mapping:[ {} ]  , settings:  [ {} ]  ", indexName , mapping, settings);
        try {
            // TODO
            CreateIndexRequest index = new CreateIndexRequest(indexName);
            index.settings(settings, settingsContentType);
            index.mapping(mapping,mappingContentType);
            client.indices().createAsync(index, RequestOptions.DEFAULT, actionListener);
        } catch (Exception e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.createMappingAsync, 创建索引映射, indexName:[ {} ], mapping:[ {} ], settings:[ {} ]", indexName , mapping,settings , e);
        }
    }

    @Override
    public boolean indexDoc(String indexName, String indexType, Map<String, Object> doc) {
        log.info("iboot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，索引一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , indexType, doc);
        /*try {
            GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，索引一篇文档:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，索引一篇文档:[ {} ] 索引不存在"
                    , indexName );
            return false;
        }*/
        IndexRequest indexRequest = new IndexRequest(indexName, indexType, (String)doc.get("id")).source(doc);
        try {
            client.index(indexRequest, RequestOptions.DEFAULT);
            return true;
        } catch(ElasticsearchException e ) {
            if (e.status() == RestStatus.CONFLICT) {
                log.error("iboot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，ElasticsearchException写入索引产生冲突，索引一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , indexType, doc, e);
            }else{
                log.error("iboot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，ElasticsearchException，索引一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , indexType, doc, e);
            }
        } catch(IOException e) {
            log.error("iboot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，IOException，索引一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , indexType, doc, e);
        } catch (Exception e){
            log.error("iboot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDoc，Exception，索引一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , indexType, doc, e);
        }
        return false;
    }


    @Override
    public boolean indexDocWithRouting(String indexName, String indexType, String route, Map<String, Object> doc) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocWithRouting,带路由索引一篇文档, indexName:[ {} ], indexType:[ {} ], route:[ {} ], doc:[ {} ]", indexName , indexType, route, doc);
        IndexRequest indexRequest = new IndexRequest(indexName, indexType, (String)doc.get("id")).source(doc);
        indexRequest.routing(route);
        try {
            client.index(indexRequest, RequestOptions.DEFAULT);
            return true;
        } catch(ElasticsearchException e ) {
            if (e.status() == RestStatus.CONFLICT) {
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocWithRouting ElasticsearchException写入索引产生冲突,带路由索引一篇文档, indexName:[ {} ], indexType:[ {} ], route:[ {} ], doc:[ {} ]", indexName , indexType, route, doc);
            }else{
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocWithRouting ElasticsearchException,带路由索引一篇文档, indexName:[ {} ], indexType:[ {} ], route:[ {} ], doc:[ {} ]", indexName , indexType, route, doc);
            }
        } catch(IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocWithRouting IOException,带路由索引一篇文档, indexName:[ {} ], indexType:[ {} ], route:[ {} ], doc:[ {} ]", indexName , indexType, route, doc);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocWithRouting Exception,带路由索引一篇文档, indexName:[ {} ], indexType:[ {} ], route:[ {} ], doc:[ {} ]", indexName , indexType, route, doc);
        }
        return false;
    }


    @Override
    public boolean indexDocs(String indexName, String indexType, List<Map<String, Object>> docs) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs,索引一组文档, indexName:[ {} ], indexType:[ {} ], docs:[ {} ]", indexName , indexType, docs);
        try {
            if (CollectionUtils.isEmpty(docs)) {
                return false;
            }
            /*GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }*/
            BulkRequest request = new BulkRequest();
            for (Map<String, Object> doc : docs) {
                request.add(new IndexRequest(indexName, indexType, (String)doc.get("id")).source(doc));
            }
            BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
            if (bulkResponse != null) {
                for (BulkItemResponse bulkItemResponse : bulkResponse) {
                    DocWriteResponse itemResponse = bulkItemResponse.getResponse();

                    if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.INDEX ||
                            bulkItemResponse.getOpType() == DocWriteRequest.OpType.CREATE) {
                        IndexResponse indexResponse = (IndexResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs,索引一组文档_新增成功！！！, indexName:[ {} ], indexType:[ {} ], indexResponse:[ {} ]", indexName , indexType, indexResponse);
                    } else if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.UPDATE) {
                        UpdateResponse updateResponse = (UpdateResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs,索引一组文档_修改成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , indexType, updateResponse);
                    } else if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.DELETE) {
                        DeleteResponse deleteResponse = (DeleteResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs,索引一组文档_删除成功！！！, indexName:[ {} ], indexType:[ {} ], deleteResponse:[ {} ]", indexName , indexType, deleteResponse);
                    }
                }
            }
            return true;
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs,IOException 索引一组文档_删除成功！！！, indexName:[ {} ], indexType:[ {} ], Response:[ {} ]", indexName , indexType, e);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocs, Exception索引一组文档_删除成功！！！, indexName:[ {} ], indexType:[ {} ], Response:[ {} ]", indexName , indexType, e);
        }
        return false;
    }


    @Override
    public boolean indexDocsWithRouting(String indexName, String indexType, List<Map<String, Object>> docs) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting,带路由索引一组文档, indexName:[ {} ], indexType:[ {} ], docs:[ {} ]", indexName , indexType, docs);
        try {
            if (CollectionUtils.isEmpty(docs)) {
                return false;
            }
            /*GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }*/
            BulkRequest request = new BulkRequest();
            for (Map<String, Object> doc : docs) {
                HashMap<String, Object> join = (HashMap<String, Object>)doc.get("joinkey");
                String route = (String)join.get("parent");
                request.add(new IndexRequest(indexName, indexType, (String)doc.get("id")).source(doc).routing(route));
            }
            BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
            if (bulkResponse != null) {
                for (BulkItemResponse bulkItemResponse : bulkResponse) {
                    DocWriteResponse itemResponse = bulkItemResponse.getResponse();

                    if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.INDEX ||
                            bulkItemResponse.getOpType() == DocWriteRequest.OpType.CREATE) {
                        IndexResponse indexResponse = (IndexResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting,带路由索引一组文档——新增成功！！！, indexName:[ {} ], indexType:[ {} ], indexResponse:[ {} ]", indexName , indexResponse);

                    } else if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.UPDATE) {
                        UpdateResponse updateResponse = (UpdateResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting,带路由索引一组文档——修改成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , updateResponse);
                    } else if (bulkItemResponse.getOpType() == DocWriteRequest.OpType.DELETE) {
                        DeleteResponse deleteResponse = (DeleteResponse) itemResponse;
                        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting,带路由索引一组文档——删除成功！！！, indexName:[ {} ], indexType:[ {} ], deleteResponse:[ {} ]", indexName , deleteResponse);
                    }
                }
                return true;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting IOException,带路由索引一组文档——删除成功！！！, indexName:[ {} ], indexType:[ {} ], deleteResponse:[ {} ]", indexName , e);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.indexDocsWithRouting Exception,带路由索引一组文档——删除成功！！！, indexName:[ {} ], indexType:[ {} ], deleteResponse:[ {} ]", indexName , e);
        }
        return false;
    }

    @Override
    public boolean deleteDoc(String indexName, String indexType, String id) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,删除一篇文档, indexName:[ {} ], indexType:[ {} ], id:[ {} ]", indexName , id);
        /*try {
            GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,删除一篇文档:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,删除一篇文档:[ {} ] 索引不存在"
                    , indexName );
            return false;
        }*/
        DeleteResponse deleteResponse = null;
        DeleteRequest request = new DeleteRequest(indexName, indexType, id);
        try {
            deleteResponse = client.delete(request, RequestOptions.DEFAULT);
            log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,删除一篇文档--删除成功！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , id, deleteResponse);
            if (deleteResponse.getResult() == DocWriteResponse.Result.NOT_FOUND) {
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,删除一篇文档--删除失败，文档不存在！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , id, deleteResponse);
                return false;
            }
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,ElasticsearchException 删除一篇文档--删除失败，删除失败，版本号冲突！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , id, deleteResponse);
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,IOException 删除一篇文档--删除失败，删除失败，版本号冲突！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , id, deleteResponse, e);
            return false;
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,Exception 删除一篇文档--删除失败，删除失败，版本号冲突！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , id, deleteResponse, e);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteDocByQuery(String indexName, String indexType, String filedName, Object value) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,按条件删除文档, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ]", indexName , filedName, JSON.toJSONString(value));
        /*try {
            GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDocByQuery,按条件删除文档:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDocByQuery,按条件删除文档:[ {} ] 索引不存在"
                    , indexName );
            return false;
        }*/
        BulkByScrollResponse deleteResponse = null;
        try {
            DeleteByQueryRequest request = new DeleteByQueryRequest(indexName);
            request.setDocTypes(indexType);
            request.setQuery(new TermQueryBuilder(filedName, value));
            request.setAbortOnVersionConflict(false);

            BulkByScrollResponse resp = client.deleteByQuery(request, RequestOptions.DEFAULT);
            long deleted = resp.getDeleted();
            log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,按条件删除文档, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ]，删除【 {} 】条文档", indexName , filedName, JSON.toJSONString(value), deleted);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,ElasticsearchException按条件删除文档--删除失败，版本号冲突！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , filedName, JSON.toJSONString(value), JSON.toJSONString(deleteResponse));
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,IOException 按条件删除文档--删除失败，IOException！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , filedName, JSON.toJSONString(value), JSON.toJSONString(deleteResponse));
            return false;
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.deleteDoc,Exception 按条件删除文档--删除失败，IOException！！！！, indexName:[ {} ], indexType:[ {} ], id:[ {} ], deleteResponse:[ {} ]", indexName , filedName, JSON.toJSONString(value), JSON.toJSONString(deleteResponse));
            return false;
        }
        return true;
    }

    @Override
    public boolean updateDoc(String indexName, String indexType, Map<String, Object> doc) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc, 修改一篇文档, indexName:[ {} ], indexType:[ {} ], doc:[ {} ]", indexName , doc);
        /*try {
            GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc, 按条件修改文档:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc, 按条件修改文档:[ {} ] 索引不存在"
                    , indexName );
            return false;
        }*/
        UpdateRequest request = new UpdateRequest(indexName, indexType, (String) doc.get("id")).doc(doc);
        request.docAsUpsert(true);
        try {
            UpdateResponse updateResponse = client.update(request, RequestOptions.DEFAULT);
            if (updateResponse.getResult() == DocWriteResponse.Result.CREATED) {
                log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc,修改一篇文档--新增成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , updateResponse);
            } else if (updateResponse.getResult() == DocWriteResponse.Result.UPDATED) {
                log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc,修改一篇文档--修改成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , updateResponse);
            } else if (updateResponse.getResult() == DocWriteResponse.Result.DELETED) {
                log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc,修改一篇文档--删除成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , updateResponse);
            } else if (updateResponse.getResult() == DocWriteResponse.Result.NOOP) {
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc,修改一篇文档--无操作！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , updateResponse);
            }
            return true;
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc IOException,修改一篇文档--删除成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , e);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDoc Exception,修改一篇文档--删除成功！！！, indexName:[ {} ], indexType:[ {} ], updateResponse:[ {} ]", indexName , e);
        }
        return false;
    }

    @Override
    public boolean updateDocByQuery(String indexName, String indexType, String filedName ,Object value, String script, Map<String, Object> doc) {
        log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, 按条件修改文档, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ], script:[ {} ], doc:[ {} ]", indexName , indexType, filedName, JSON.toJSONString(value), script, JSON.toJSONString(doc));
        BulkByScrollResponse updateResponse = null;
        /*try {
            GetIndexRequest exists = new GetIndexRequest(indexName);
            if (!client.indices().exists(exists, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, 按条件修改文档:[ {} ] 索引不存在"
                        , indexName );
                return false;
            }
        } catch (IOException e) {
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, 按条件修改文档:[ {} ] 索引不存在"
                    , indexName );
            return false;
        }*/
        try {
            UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest(indexName);
            updateByQueryRequest.setDocTypes(indexType);
            updateByQueryRequest.setQuery(new TermQueryBuilder(filedName, value));
            updateByQueryRequest.setAbortOnVersionConflict(false);
            //设置版本冲突时继续
            updateByQueryRequest.setConflicts("proceed");
            updateByQueryRequest.setScript(
              new Script(
                      ScriptType.INLINE, "painless", script,doc
              )
            );

            updateResponse = client.updateByQuery(updateByQueryRequest, RequestOptions.DEFAULT);
            log.info("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, 按条件修改文档完成, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ], script:[ {} ], doc:[ {} ], updateResponse:[ {} ]", indexName , indexType, filedName, JSON.toJSONString(value), script, JSON.toJSONString(doc), JSON.toJSONString(updateResponse));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, IOException 按条件修改文档失败, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ], script:[ {} ], doc:[ {} ], updateResponse:[ {} ]", indexName , indexType, filedName, JSON.toJSONString(value), script, JSON.toJSONString(doc), JSON.toJSONString(updateResponse), e);
        }catch (Exception e){
            e.printStackTrace();
            log.error("boot.spring.elastic.service.impl.SimpleIndexServiceImpl.updateDocByQuery, Exception 按条件修改文档失败, indexName:[ {} ], indexType:[ {} ], filedName:[ {} ], value:[ {} ], script:[ {} ], doc:[ {} ], updateResponse:[ {} ]", indexName , indexType, filedName, JSON.toJSONString(value), script, JSON.toJSONString(doc), JSON.toJSONString(updateResponse), e);
        }
        return false;
    }
}

package boot.spring.elastic.service.impl;

import boot.spring.elastic.service.SimpleSearchService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;

import java.io.IOException;

@Slf4j
public class SimpleSearchServiceImpl implements SimpleSearchService {


    private RestHighLevelClient client;

    public SimpleSearchServiceImpl(RestHighLevelClient client){
        this.client = client;
    }

    @Override
    public SearchResponse search(SearchRequest searchRequest) {
        log.info("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.search, searchRequest:[ {} ]"
                , searchRequest );
        try {
            /*GetIndexRequest request = new GetIndexRequest(searchRequest.indices());
            if (!client.indices().exists(request, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.search, searchRequest:[ {} ] 索引不存在"
                        , searchRequest );
                return null;
            }*/
            return client.search(searchRequest, RequestOptions.DEFAULT);
        }catch (IOException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.search IOException, searchRequest:[ {} ]"
            , searchRequest , e);
        }catch (ElasticsearchException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.search ElasticsearchException, countRequest:[ {} ]"
                    , searchRequest , e);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.search Exception, searchRequest:[ {} ]"
                    , searchRequest , e);
        }
        return null;
    }

    @Override
    public MultiGetResponse batchSearch(MultiGetRequest searchRequest) {
        log.info("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.batchSearch, searchRequest:[ {} ]"
                , searchRequest );
        try {
            return client.mget(searchRequest, RequestOptions.DEFAULT);
        }catch (IOException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.batchSearch IOException, searchRequest:[ {} ]"
                    , searchRequest , e);
        }catch (ElasticsearchException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.batchSearch ElasticsearchException, countRequest:[ {} ]"
                    , searchRequest , e);
        } catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.batchSearch Exception, searchRequest:[ {} ]"
                    , searchRequest , e);
        }
        return null;
    }

    @Override
    public SearchResponse scroll(SearchScrollRequest searchScrollRequest) {
        log.info("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.scroll, searchScrollRequest:[ {} ]"
                , searchScrollRequest );
        try {
            return client.scroll(searchScrollRequest, RequestOptions.DEFAULT);
        }catch (IOException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.scroll IOException, searchScrollRequest:[ {} ]"
                    , searchScrollRequest , e);
        }catch (ElasticsearchException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.scroll ElasticsearchException, countRequest:[ {} ]"
                    , searchScrollRequest , e);
        }catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.scroll Exception, searchScrollRequest:[ {} ]"
                    , searchScrollRequest , e);
        }
        return null;
    }


    @Override
    public boolean clearScroll(String scrollId) {
        log.info("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.clearScroll, scrollId:[ {} ]"
                , scrollId );
        // 完成滚动后，清除滚动上下文
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        try {
            ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
            return clearScrollResponse.isSucceeded();
        }catch (IOException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.clearScroll, scrollId:[ {} ] IOException"
                    , scrollId, e);
        }catch (ElasticsearchException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.clearScroll, scrollId:[ {} ] ElasticsearchException"
                    , scrollId, e);
        }catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.clearScroll, scrollId:[ {} ] Exception"
                    , scrollId, e);
        }
        return false;
    }

    @Override
    public long count(CountRequest countRequest) {
        log.info("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.count, countRequest:[ {} ]"
                , countRequest );
        try {
           /* String[] indices = countRequest.indices();
            GetIndexRequest request = new GetIndexRequest(indices);
            if (!client.indices().exists(request, RequestOptions.DEFAULT)){
                log.warn("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.count, countRequest:[ {} ] 索引不存在"
                        , countRequest );
                return 0;
            }*/
            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
            return countResponse.getCount();
        }catch (IOException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.count IOException, countRequest:[ {} ]"
                    , countRequest , e);
        }catch (ElasticsearchException e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.count ElasticsearchException, countRequest:[ {} ]"
                    , countRequest , e);
        }catch (Exception e){
            log.error("boot.spring.elastic.service.impl.SimpleSearchServiceImpl.count Exception, countRequest:[ {} ]"
                    , countRequest , e);
        }
        return 0;
    }
}

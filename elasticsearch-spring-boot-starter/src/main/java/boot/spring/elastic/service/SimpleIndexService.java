package boot.spring.elastic.service;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;

import java.util.List;
import java.util.Map;

public interface SimpleIndexService {

    /**
     * 判断一个索引是否存在
     * @param indexName
     * @return
     */
    boolean existIndex(String indexName);

    /**
     * 删除索引
     * @param indexName
     * @return
     */
    boolean deleteIndex(String indexName);
    /**
     * 创建索引映射
     * @param indexName
     * @param mapping
     * @return
     */
    boolean createMapping(String indexName, XContentBuilder mapping);

    /**
     * 创建索引映射
     * @param indexName 索引名称
     * @param mapping mapping字符串
     * @param xContentType 类型
     * @param settings settings
     * @param settingsContentType
     * @return
     */
    boolean createMapping(String indexName, String mapping, XContentType xContentType, String settings, XContentType settingsContentType);

    /**
     * 异步创建索引映射
     * @param indexName 索引名称
     * @param mapping mapping
     * @param actionListener 响应回调
     */
    void createMappingAsync(String indexName, XContentBuilder mapping, ActionListener actionListener);


    /**
     * 异步创建索引映射
     * @param indexName 索引名称
     * @param mapping mapping
     * @param settings settings
     * @param settingsContentType
     * @param actionListener 响应回调
     */
    void createMappingAsync(String indexName,  String mapping, XContentType xContentType, String settings, XContentType settingsContentType, ActionListener actionListener);

    /**
     * 删除一篇文档
     * @param indexName
     * @param indexType
     * @param id
     * @return
     */
    boolean deleteDoc(String indexName, String indexType, String id);

    /**
     * 删除一篇文档
     * @param indexName
     * @param indexType
     * @param filedName
     * @param value
     * @return
     */
    boolean deleteDocByQuery(String indexName, String indexType, String filedName ,Object value);

    /**
     * 修改一篇文档
     * @param indexName
     * @param indexType
     * @param doc
     * @return
     */
    boolean updateDoc(String indexName, String indexType, Map<String, Object> doc);

    /**
     * 按条件更新文档
     * @param indexName
     * @param indexType
     * @param filedName
     * @param value
     * @param script
     * @param doc
     * @return
     */
    boolean updateDocByQuery(String indexName, String indexType, String filedName ,Object value, String script, Map<String, Object> doc);
    /**
     * 索引一篇文档
     * @param indexName
     * @param indexType
     * @param doc
     * @return
     */
    boolean indexDoc(String indexName, String indexType, Map<String, Object> doc);

    /**
     * 带路由索引一篇文档
     * @param indexName
     * @param indexType
     * @param route
     * @param doc
     * @return
     */
    boolean indexDocWithRouting(String indexName, String indexType, String route, Map<String, Object> doc);

    /**
     * 索引一组文档
     * @param indexName
     * @param indexType
     * @param docs
     * @return
     */
    boolean indexDocs(String indexName, String indexType, List<Map<String, Object>> docs);

    /**
     * 带路由索引一组文档
     * @param indexName
     * @param indexType
     * @param docs
     * @return
     */
    boolean indexDocsWithRouting(String indexName, String indexType, List<Map<String, Object>> docs);

}

package boot.spring.elastic.service;

import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.core.CountRequest;

public interface SimpleSearchService {

    SearchResponse search(SearchRequest searchRequest);

    MultiGetResponse batchSearch(MultiGetRequest request);

    SearchResponse scroll(SearchScrollRequest searchScrollRequest);

    boolean clearScroll(String scrollId);

    long count(CountRequest countRequest);
}

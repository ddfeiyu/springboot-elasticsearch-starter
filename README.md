# springboot-elasticsearch-starter

#### 介绍
 
 ## 前置
 
 [使用Java High Level REST Client操作elasticsearch ](https://www.cnblogs.com/ginb/p/8716485.html)
 
 # springboot-elasticsearch-starter
 
 ### 介绍
 
 Spring boot整合elastic search 6.8.1实现全文检索。
 
 主要包含以下特性：
 
 1. 全文检索的实现主要包括构建索引、高级搜索、聚集统计、数据建模四个模块；
 
 2. 使用 **elasticsearch-rest-high-level-client** 来操作elasticsearch，构建索引时，根据实际情况考虑哪些字段需要分词，哪些不需要分词，这会影响搜索结果。
 
    使用IK分词器虽然能解决一些中文分词的问题，但是由于分词的粒度不够细，导致很多词语可能搜不到。
    
    例如IK分词器在构建索引“三国无双”时，会把“三国”“无双”存起来建索引，但是搜索“国无”时，搜不出来，因此，我们采用把文本拆分到最细粒度来进行分词，从而最大限度地搜索到相关结果。
    
    详情参考：[如何手动控制分词粒度提高搜索的准确性](https://gitee.com/shenzhanwang/Spring-elastic_search/wikis/%E5%A6%82%E4%BD%95%E6%89%8B%E5%8A%A8%E6%8E%A7%E5%88%B6%E5%88%86%E8%AF%8D%E7%B2%92%E5%BA%A6%E6%8F%90%E9%AB%98%E6%90%9C%E7%B4%A2%E7%9A%84%E5%87%86%E7%A1%AE%E6%80%A7?sort_id=1727039)
 
 3. 高级搜索实现了以下几种：
     - 多字段搜索,指定多个字段进行搜索:query_string，支持高亮显示
     - 经纬度搜索:distanceQuery
     - 范围过滤,对搜索结果进一步按照范围进行条件过滤：rangeQuery
 
 4. 搜索结果的展示提供了普通分页和滚动分页两种实现。
 
    - 普通分页只适合数据量较小的场景，在数据量非常大的情况下，start+size普通分页会把全部记录加载到内存中，这样做不但运行速递特别慢，而且容易让es出现内存不足而挂掉。
    - 滚动分页需要得到一个scrollid，以后每次使用scrollid去获取下一页的内容，缺点是不能跳页。
 
 5. 聚集统计包含词条聚集、日期直方图聚集、范围聚集，并使用chart.js进行可视化
 
 6. 数据建模部分实现了嵌套对象的使用，查询时无需join性能较好，但是在建索引时就要把关联数据join好嵌套进去。
 
 7. swagger入口：http://localhost:8080/swagger-ui.html
  

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
